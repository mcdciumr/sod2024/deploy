# Continuous _Deployment_

Die Auslieferung von Software auf Produktivsysteme kann mit Pipelines automatisiert werden,
so dass jede Änderung, die geprüft und übernommen wurde (z.B. durch Pipelines an anderen
Stellen), unmittelbar und ohne manuelles Zutun an die Nutzer ausgeliefert wird.

Für die Auslieferung sind in der Regel vertrauliche Informationen nötig, wie Passwörter
und API Keys. Diese sollten nicht in der Konfigdatei oder den Pipeline-Logs auftauchen.
Dafür existiert das Konzept von CI/CD-Variablen.

```yaml
image: ubuntu:22.04

default:
  cache:
    key: build-cache
    paths:
      - butler/

butler:
  before_script:
    - apt-get update && apt-get install -y curl unzip
  script:
    - curl -L -o butler.zip https://broth.itch.ovh/butler/linux-amd64/LATEST/archive/default
    - unzip -o butler.zip -d butler
    - chmod +x butler/butler

itch:
  before_script:
    - apt-get update && apt-get install -y ca-certificates
  script:
    - ./butler/butler push game mcdci-levelup/gitlab-cicd-demo:html5
```

- Zugangsdaten sicher aufbewahren:
  - Problem 1: Wenn Keys in der CI-Konfigdatei stehen, können sie von allen gelesen werden, die Zugriff auf das Repo haben
  - Problem 2: Wenn Keys in der CI-Pipeline verwendet werden, können sie in Logs auftauchen
  - Problem 3: Wenn ein CI-Script bösartigen Code enthält, können Keys ausgelesen und an andere Webseiten verschickt werden
  - Lösung Problem 1: CI-Variablen verwenden (**Settings -> CI/CD -> Variables**)
  - Lösung Problem 2: CI-Variablen maskieren und ggfs. Branch Protection aktivieren
  - Lösung Problem 3: Code Reviews (+ Merge Requests und Branch Protection)
- Beispiel Itch: CMD-Tool `butler` verwendet API Key `BUTLER_API_KEY`
  - API-Key ist hinterlegt und kann nur von Leuten mit entsprechenden Rechten gesehen und geändert werden (Maintainer/Owner)
  - Weil das Tool automatisch nach einem Key mit diesem namen sucht, taucht nicht einmal die Variable in der Pipeline auf
- Heruntergeladene Dateien können in Cache gespeichert werden, um in mehreren Jobs zur Verfügung zu stehen und nicht
  erneut heruntergeladen werden zu müssen (`default: cache`). 
- `before_script`: kann zur Ausführung von Setup-Befehlen genutzt werden. Hier: Installation von Paketen, die zum 
  Download (`curl`) und Entpacken von Zip-Dateien (`unzip`) benötigt werden.
